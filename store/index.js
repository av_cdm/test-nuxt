import { convertToFormData } from '@/utils/common'

export const state = () => ({
  user: null,
  projects: [],
  error: ''
})

export const mutations = {
  SET_USER(state, user) {
    state.user = user
  },
  SET_PROJECTS(state, projects) {
    state.projects = projects
  },
  UPDATE_PROJECT_NAME(state, project) {
    state.projects = [...state.projects.map(i => i.id === project.id ? project : i)]
  },
  SET_ERROR(state, error) {
    state.error = error
  }
}

export const getters = {
  user(state) {
    return state.user
  },
  projects(state) {
    return state.projects
  }
}

export const actions = {
  nuxtServerInit({ commit }) { },
  setUser({ commit }, data) {
    commit('SET_USER', data)
  },
  updateProjectName({ commit }, data) {
    commit('UPDATE_PROJECT_NAME', data)
  },
  async fetchProjects({ commit }) {
    try {
      commit('SET_ERROR', '')
      const data = await this.$axios.$get('projects-manage/index')
      commit('SET_PROJECTS', data.projects)
    } catch (e) {
      commit('SET_PROJECTS', [])
      commit('SET_ERROR', getResponseError(e))
    }
  },
  async updateProject({ commit }, data) {
    try {
      commit('SET_ERROR', '')
      await this.$axios.$post(`projects-manage/update?id=${data.id}`, convertToFormData({ ...data }), { 'Content-Type': 'multipart/form-data' })
      commit('UPDATE_PROJECT_NAME', data)
    } catch (e) {
      // commit('UPDATE_PROJECT_NAME', data)
      commit('SET_ERROR', getResponseError(e))
    }
  }
}

function getResponseError(e) {
  console.error('Original error -->', e)
  if (e && e.response && e.response.data && e.response.data.message) {
    const errors = e.response.data.message
    if (Array.isArray(errors)) {
      return Object.keys(errors).map(i => errors[i]).join('<br />')
    }
    return errors
  }
  return 'Something went wrong =('
}
