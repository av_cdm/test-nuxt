export const getTimeSpent = (time = 0) => {
  const date = new Date(0)
  date.setSeconds(time)
  return date.toISOString().substr(11, 8)
}

export const convertToFormData = (obj, form, fieldNameSpace) => {
  const formData = form || new FormData()
  let formKey

  Object.keys(obj).forEach((property) => {
    if (fieldNameSpace) {
      formKey = `${fieldNameSpace}[${property}]`
    } else {
      formKey = property
    }
    // if the property is an object, but not a File,
    // map recursively.
    if (typeof obj[property] === 'object' && obj[property] !== null && !(obj[property] instanceof File)) {
      convertToFormData(obj[property], formData, formKey)
    } else {
      // if it's a string or a File object
      formData.append(formKey, obj[property])
    }
  })
  return formData
}
